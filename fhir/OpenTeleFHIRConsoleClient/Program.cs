﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hl7.Fhir.Model;
using Hl7.Fhir.Rest;
using Hl7.Fhir.Support;
using Hl7.Fhir.Introspection;
using Hl7.Fhir.Serialization;
using Hl7.Fhir.Validation;

namespace OpentTeleFHIRConsoleClient
{
    class Program
    {
        static void Main(string[] args)
        {
            var client = new FhirClient("http://localhost:8080/fhir");
            //var client = new FhirClient("http://nprogram.azurewebsites.net/");
            //var client = new FhirClient("http://fhir2.healthintersections.com.au/open/");
            //var pat = client.Read<Patient>("Patient/1");
            //var pat = client.Read<Patient>("Patient/1?_format=xml");
            DateTimeOffset dtoffset = new DateTimeOffset();
            dtoffset.AddDays(2);

            var bundle = client.Read<Bundle>("Patient/weight");
            Console.WriteLine("Welcome to the Fhir client!");
            int length = bundle.Entry.Count;
            for (int i = 0; i < length; i++)
            {

                Observation observation = (Observation)bundle.Entry[i].Resource;
                Quantity quantity = (Quantity)observation.Value;
                Console.WriteLine("Weight: " + quantity.ValueElement.Value);
            }

            Console.ReadLine();
        }
    }
}

