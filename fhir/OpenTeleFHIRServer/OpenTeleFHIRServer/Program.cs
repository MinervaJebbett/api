﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel.Web;
using HtmlAgilityPack;

namespace OpenTeleFHIR
{
    class Program
    {
        static void Main(string[] args)
        {
            WebServiceHost host = new WebServiceHost(typeof(FHIRService),
                new Uri("http://localhost:8080/fhir"));
            host.Open();
            Console.Write("Welcome to the FHIR server!");

            //Scraper scraper = new Scraper();
            //Dictionary<String, String> patientData = scraper.Scrape();
            //Console.WriteLine(string.Format("Span id={0}, Span text={1}", patientData[(String)"Brugernavn"], patientData[(String)"Brugernavn"]));
            Console.ReadLine();
        }
    }
}
