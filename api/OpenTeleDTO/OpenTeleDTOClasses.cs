﻿/*
 * Copyright 2015 the original authors Stefan Wagner 
 * Contact sw@eng.au.dk for additional information.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 * Introduction (Author Stefan Wagner):
 * This is the first version of a C# based .NET wrapper for the OpenTele2 web API.
 * Focus is on the patient API, but work is also commencing on wrapping the
 * clinical interface. The patient API allows for getting and sending questionnaire data belonging to a patient:
 * Questionnaire data, automated or manual medical device measurements, and subjective observations, 
 * as well as for exchanging text messages between the clinicians, as well as 
 * The OpenTele project is made to promote the use of telemedicine, by illustrating how monitoring and training
 * can happen remotely. It is part of the national Danish strategy for telemedicine, and it is being used in
 * such diverse areas as pre-eclampsia monitoring, COPD, diabets, and patients suffering from depression 
 * and dementia. This .NET API has been created to support moving to other platforms, including Windows .NET 
 * and Linux Mono.Net frameworks, to supplement the current Android and HTML5 based clients. 
 * The OpenTele2 project is open source under the Apache 2.0 license - and thus this wrapper falls under the 
 * license to be fully compatiable. Creating alternative user interfaces for both patients and clinicans is 
 * important, as well as for integrating with third party medical devices and systems. Thus, we are also working 
 * on HL7 integration (FHIR and more).
 * The provided solution include a source project for data transfer objects called "OpenTele.DTO". 
 * This builds to an assembly (dll) called "OpenTele.DTO.dll". This dll may be shared across
 * different layers (GUI, BLL, DAL) but may also be used for creating distributed wrappers, e.g. HL7 FHIR based
 * wrappers. Basically, use this for domain understanding. However, OpenTeleNet.API also allows for raw JSON style  * communication as an alternative. Thus, most methods exists in both a raw JSON format, as well as an 
 * object oriented (data transfer object) approach.
 * The core of the API is the OpenTeleNet.API project and the resulting assembly "OpenTeleNet.API.dll".
 * Also in the API you can find the OpenTeleConsoleDemo project - which will provide an executable that will demo
 * how to use the API. The OpenTeleConsoleDemo project uses the two dlls "OpenTele.DTO.dll" and
 * "OpenTeleNet.API.dll" to work. 
 * For more information on OpenTele2 - please visit: http://4s-online.dk/wiki/doku.php?id=opentele:overview
 * The authors retain all rights to use the provided code and concepts for other purposes, both acadmic,  
 * teching and research, and commercial use.
 * In case of questions, please contact Stefan Wagner at sw@eng.au.dk
 * 
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//This name space includes the data transfer objects in OpenTeleNet (SW)
//It is used to model the OpenTele2 JSON encoding format.
//Naming is uncoventional due to bad OpenTele2 naming. 
namespace OpenTele.DTO
{
    //http://json2csharp.com/


    public class QuestionnarieRootObject
    {
        public Questionnaire[] questionnaires { get; set; }
    }

    public class Questionnaire
    {
        public int id { get; set; }
        public string name { get; set; }
        public object schedule { get; set; }
        public string version { get; set; }
    }

    /// <summary>
    /// The Measurement class provides the .NET mapping for OpenTel2 JSON measurement encoding
    /// Author: Stefan Wagner
    /// </summary>
    public class Measurement
    {
        public string name { get; set; }
        public Links links { get; set; }
        public string timestamp { get; set; }
        public BloodSugarMeasurement measurement { get; set; }
        public DateTime timestamp2 { get; set; }
        public float value { get; set; }
    }

    /// <summary>
    /// Needed by OpenTele2 JSON encoding.
    /// Author: Stefan Wagner
    /// </summary>
    public class Links
    {
        public string measurement { get; set; }
        public string self { get; set; }

    }


    /// <summary>
    /// Blood sugar is a complex measurement type.
    /// Author: Stefan Wagner
    /// </summary>
    public class BloodSugarMeasurement
    {
        public double value { get; set; }
        public object isAfterMeal { get; set; }
        public bool isBeforeMeal { get; set; }
        public object isControlMeasurement { get; set; }
        public object isOutOfBounds { get; set; }
        public object otherInformation { get; set; }
        public object hasTemperatureWarning { get; set; }
    }

    /// <summary>
    /// Required by OpenTele JSON
    /// Author: Stefan Wagner
    /// </summary>
    public class Links2
    {
        public string self { get; set; }
    }



    /// <summary>
    /// This is the main class mapping to the OpenTele2 JSON format 
    /// used with complex measurements - such as blood sugar.
    /// Author. Stefan Wagner
    /// </summary>
    public class ComplexMeasurementsRootObject
    {
        public string type { get; set; }
        public object unit { get; set; }
        public List<Measurement> measurements { get; set; }
        public Links links { get; set; }
        public List<Questionnaire> questionnaires { get; set; }
        public int QuestionnaireId { get; set; }
        public string name { get; set; }
        public string version { get; set; }
        public Output[] output { get; set; }
        public DateTime date { get; set; }

    }

    /// <summary>
    /// This is a helper class 
    /// used with complex measurements - such as blood sugar.
    /// Author. Stefan Wagner
    /// </summary>
    public class Output
    {
        public string name { get; set; }
        public string type { get; set; }
        public object value { get; set; }
    }


    /// <summary>
    /// A specialized root object class for weight measurements.
    /// Root objects are the "root" of a JSON string
    /// Author: Stefan Wagner
    /// </summary>
    public class WeightRootObject
    {
        public string type { get; set; }
        public string unit { get; set; }
        public WeightMeasurement[] measurements { get; set; }
        public Links links { get; set; }
    }

    /// <summary>
    /// The detailed dto for weight measurements
    /// </summary>
    public class WeightMeasurement
    {
        public DateTime timestamp { get; set; }
        public float measurement { get; set; }
    }

    /// <summary>
    /// A specialized root object class for saturation measurements.
    /// Root objects are the "root" of a JSON string
    /// Author: Stefan Wagner
    /// </summary>
    public class SaturationRootobject
    {
        public string type { get; set; }
        public string unit { get; set; }
        public SaturationMeasurement[] measurements { get; set; }
        public Links links { get; set; }
    }

    /// <summary>
    /// The detailed dto for saturation measurement types
    /// </summary>
    public class SaturationMeasurement
    {
        public DateTime timestamp { get; set; }
        public float measurement { get; set; }
    }

    /// <summary>
    /// A general root object class for basic measurement types, such as pulse, weight, saturaiton.
    /// Root objects are the "root" of a JSON string
    /// Author: Stefan Wagner
    /// </summary>
    public class BasicMeasurementRootobject
    {
        public string type { get; set; }
        public string unit { get; set; }
        public BasicMeasurement[] measurements { get; set; }
        public Links links { get; set; }
    }


    /// <summary>
    /// The detailed dto for basic measurement types
    /// </summary>
    public class BasicMeasurement
    {
        public DateTime timestamp { get; set; }
        public float measurement { get; set; }
    }

    /// <summary>
    /// A specialized root object class for blood pressure measurements.
    /// Root objects are the "root" of a JSON string
    /// Author: Stefan Wagner
    /// </summary>
    public class BloodPressureRootobject
    {
        public string type { get; set; }
        public string unit { get; set; }
        public BloodPressureMeasurement[] measurements { get; set; }
        public Links links { get; set; }
    }

    /// <summary>
    /// The detailed dto for blood pressure measurement types
    /// Note - it links to a sub string
    /// Author: Stefan Wagner
    /// </summary>
    public class BloodPressureMeasurement
    {
        public DateTime timestamp { get; set; }
        public BloodPressureMeasurementDetails measurement { get; set; }
    }

    /// <summary>
    /// The details of a blood pressure measurement - not including pulse???
    /// Thus, always remember, that the pulse is a major component when understanding blood pressure.
    /// Thus, this must be handled at the application level.
    /// Author: Stefan Wagner
    /// </summary>
    public class BloodPressureMeasurementDetails
    {
        public float systolic { get; set; }
        public float diastolic { get; set; }
    }


    /// <summary>
    /// A specialized root object class for exchanging messages between patients and clinicans.
    /// Root objects are the "root" of a JSON string
    /// Author: Stefan Wagner
    /// </summary>
    public class MessageRootobject
    {
        public int unread { get; set; }
        public Message[] messages { get; set; }
    }

    /// <summary>
    /// The dto class for a message.
    /// Author: Stefan Wagner
    /// </summary>
    public class Message
    {
        public int id { get; set; }
        public object title { get; set; }
        public string text { get; set; }
        public To to { get; set; }
        public From from { get; set; }
        public bool isRead { get; set; }
        public DateTime sendDate { get; set; }
        public DateTime? readDate { get; set; }
    }

    /// <summary>
    /// Detail of message - 
    /// To: the recipient of the message
    /// </summary>
    public class To
    {
        public string type { get; set; }
        public int id { get; set; }
        public string name { get; set; }
    }

    /// <summary>
    /// Detail of message - 
    /// From: the recipient of the message
    /// </summary>
    public class From
    {
        public string type { get; set; }
        public int id { get; set; }
        public string name { get; set; }
    }

}
