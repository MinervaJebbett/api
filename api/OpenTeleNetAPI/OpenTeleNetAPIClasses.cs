﻿/*
 * Copyright 2015 the original authors Stefan Wagner 
 * Contact sw@eng.au.dk for additional information.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 * Introduction (Author Stefan Wagner):
 * This is the first version of a C# based .NET wrapper for the OpenTele2 web API.
 * Focus is on the patient API, but work is also commencing on wrapping the
 * clinical interface. The patient API allows for getting and sending questionnaire data belonging to a patient:
 * Questionnaire data, automated or manual medical device measurements, and subjective observations, 
 * as well as for exchanging text messages between the clinicians, as well as 
 * The OpenTele project is made to promote the use of telemedicine, by illustrating how monitoring and training
 * can happen remotely. It is part of the national Danish strategy for telemedicine, and it is being used in
 * such diverse areas as pre-eclampsia monitoring, COPD, diabets, and patients suffering from depression 
 * and dementia. This .NET API has been created to support moving to other platforms, including Windows .NET 
 * and Linux Mono.Net frameworks, to supplement the current Android and HTML5 based clients. 
 * The OpenTele2 project is open source under the Apache 2.0 license - and thus this wrapper falls under the 
 * license to be fully compatiable. Creating alternative user interfaces for both patients and clinicans is 
 * important, as well as for integrating with third party medical devices and systems. Thus, we are also working 
 * on HL7 integration (FHIR and more).
 * The provided solution include a source project for data transfer objects called "OpenTele.DTO". 
 * This builds to an assembly (dll) called "OpenTele.DTO.dll". This dll may be shared across
 * different layers (GUI, BLL, DAL) but may also be used for creating distributed wrappers, e.g. HL7 FHIR based
 * wrappers. Basically, use this for domain understanding. However, OpenTeleNet.API also allows for raw JSON style  * communication as an alternative. Thus, most methods exists in both a raw JSON format, as well as an 
 * object oriented (data transfer object) approach.
 * The core of the API is the OpenTeleNet.API project and the resulting assembly "OpenTeleNet.API.dll".
 * Also in the API you can find the OpenTeleConsoleDemo project - which will provide an executable that will demo
 * how to use the API. The OpenTeleConsoleDemo project uses the two dlls "OpenTele.DTO.dll" and
 * "OpenTeleNet.API.dll" to work. 
 * For more information on OpenTele2 - please visit: http://4s-online.dk/wiki/doku.php?id=opentele:overview
 * The authors retain all rights to use the provided code and concepts for other purposes, both acadmic,  
 * teching and research, and commercial use.
 * In case of questions, please contact Stefan Wagner at sw@eng.au.dk
 * 
 */
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft;
using OpenTele.DTO;

// The namespace for OpenTeleNet C# based wrapper API
namespace OpenTeleNet.API
{
    /// <summary>
    /// This is the main entrance to the OpenTeleNet API.
    /// It is basically a wrapper of most existing OpenTele2 web services and web interfaces
    /// Also, it is a facade to the a range of functionality in the API and in OpenTele.
    /// Author: Stefan Wagner
    /// </summary>
    public class OpenTeleNetWrapperFacade
    {
        // The below static strings have been collected from various parts of the OpenTele documentation 
        // and from reverse engineering of http calls. There are more interfaces that have not yet been mapped.
        // Consider refactoring this to a property file or similar (SW) 
        static string MeasurementListURL = @"patient/measurements";
        static string BloodPressureMeasurementListURL = @"patient/measurements/blood_pressure";
        static string BloodSugarURL = @"patient/measurements/bloodsugar";
        static string LoginAndQuestionnaireListURL = @"rest/questionnaire/listing";
        static string TemperatureURL = @"patient/measurements/temperature";
        static string UrineURL = @"patient/measurements/urine";
        static string PulseURL = @"patient/measurements/pulse";
        static string WeightURL = @"patient/measurements/weight";
        static string SaturationURL = @"patient/measurements/saturation";
        static string LungfunctionURL = @"patient/measurements/lung_function";
        static string MessageList = @"rest/message/list/";

        //Property for keeping information on the current server used
        private string Server { get; set; }

        /// <summary>
        /// Constructor taking a server endpoint as a string.
        /// Always use this contructor to create a new API wrapper.
        /// Author: Stefan Wagner
        /// </summary>
        /// <param name="server">endpoint formattet as a string (e.g. http://opentele-citizen.4s-online.dk/)
        /// </param>
        public OpenTeleNetWrapperFacade(string server)
        {
            Server = server;
        }
        
        /// <summary>
        /// Performs a Login and Returns the Questionnaire attached to the patient.
        /// Please note, that in OpenTele, a Questionnarie is also used as a menu builder, 
        /// including in the OpenTele2 Android and HTML5 versions.
        /// Author: Stefan Wagner.
        /// </summary>
        /// <param name="username">the username of the user wishing to login (e.g. nancyann)</param>
        /// <param name="password">the password of the user (e.g. abcd1234)</param>
        /// <returns>a JSON structure containing the questionnaire attached to the patient</returns>
        public string LoginRaw(string username, string password) 
        {
            return CallOpenTeleHttpAPI(LoginAndQuestionnaireListURL, true, username, password);
        }

        /// <summary>
        /// Performs a Login and Returns the Questionnaire attached to the patient - serialized into 
        /// an object structure that is easy to pass as data transfer objects.
        /// Please note, that in OpenTele, a Questionnarie is also used as a menu builder, 
        /// including in the OpenTele2 Android and HTML5 versions.
        /// Author: Stefan Wagner.
        /// </summary>
        /// <param name="username">the username of the user wishing to login (e.g. nancyann)</param>
        /// <param name="password">the password of the user (e.g. abcd1234)</param>
        /// <returns>an object structure serialized from json containing the questionnaire attached to patient
        /// </returns>        
        public ComplexMeasurementsRootObject Login(string username, string password)
        {
            //Call the raw version and deserialize into the object structure
            return (Newtonsoft.Json.JsonConvert.DeserializeObject<ComplexMeasurementsRootObject>
                (LoginRaw(username, password)));
        }

        /// <summary>
        /// Will provide a list of messages from and/or to the patient.
        /// THus, this is the direct non-verbal communication with the patient. 
        /// Author: Stefan Wagner.
        /// </summary>
        /// <param name="username">the username of the user wishing to login (e.g. nancyann)</param>
        /// <param name="password">the password of the user (e.g. abcd1234)</param>
        /// <returns>A raw JSON encoded list of messages</returns>
        public string GetMessageListRaw(string username, string password)
        {
            return CallOpenTeleHttpAPI(MessageList, false, username, password);

        }

        /// <summary>
        /// Will provide a list of messages from and/or to the patient.
        /// Thus, this is the direct non-verbal communication with the patient.
        /// Author: Stefan Wagner.        
        /// </summary>
        /// <param name="username">the username of the user wishing to login (e.g. nancyann)</param>
        /// <param name="password">the password of the user (e.g. abcd1234)</param>
        /// <returns>A serialized object structure of messages.</returns>
        public MessageRootobject GetMessageList(string username, string password)
        {
            //Call the raw JSON version and deserialize into the object structure
            return (Newtonsoft.Json.JsonConvert.DeserializeObject<MessageRootobject>
                (GetMessageListRaw(username, password)));
        }

        /// <summary>
        /// Will provide a list of all measurement types.
        /// Author: Stefan Wagner.        
        /// </summary>
        /// <param name="username">the username of the user wishing to login (e.g. nancyann)</param>
        /// <param name="password">the password of the user (e.g. abcd1234)</param>
        /// <returns>A JSON object structure of messages.</returns>
        public string GetMeasurementListRaw(string username, string password)
        {
            return CallOpenTeleHttpAPI(MeasurementListURL, false, username, password);

        }

        /// <summary>
        /// Will provide a list of all measurement types.
        /// Author: Stefan Wagner.        
        /// </summary>
        /// <param name="username">the username of the user wishing to login (e.g. nancyann)</param>
        /// <param name="password">the password of the user (e.g. abcd1234)</param>
        /// <returns>A serialized object structure of messages.</returns>
        public ComplexMeasurementsRootObject GetMeasurementList(string username, string password)
        {

            return (Newtonsoft.Json.JsonConvert.DeserializeObject<ComplexMeasurementsRootObject>
                (GetMeasurementListRaw(username, password)));
        }

        /// <summary>
        /// This will call the OpenTeleHTTPAPI and return a JSON response (SW)
        /// Author: Stefan Wagner
        /// </summary>
        /// <param name="url">the endpoint url to call </param>
        /// <param name="username">the username of the user wishing to login (e.g. nancyann)</param>
        /// <param name="password">the password of the user (e.g. abcd1234)</param>
        /// <returns>a JSON string with the result</returns>
        private string CallOpenTeleHttpAPI(string url, bool rest, string username, string password)
        {
            // The web request wraps the http communication needed to talk to OpenTele server
            WebRequest request = WebRequest.Create(Url(url));
            // Will format the http communication with the proper credentials (using basic authentication)
            request.Credentials = GetCredential(rest, url, username, password);
            request.PreAuthenticate = true;
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            Stream receiveStream = response.GetResponseStream();
            // Pipes the stream to a higher level stream reader with the required encoding format. 
            // Consider if this encoding is the right one (e.g. what does the server use?) (SW). 
            StreamReader readStream = new StreamReader(receiveStream, Encoding.Default);
            String data = readStream.ReadToEnd();
            
            data = DoWellKnownCleanUp(data);
            return data;

        }

        
        /// <summary>
        /// From version 2.0.2 to 2.12.0 there has been some strange changes in the system. This will 
        /// repair some of them. TODO: Check with 4S whether they now what is wrong.
        /// </summary>
        /// <param name="data"></param>
        private string DoWellKnownCleanUp(string data)
        {
            data = data.Replace("\"isAfterMeal\":null", "\"isAfterMeal\":false");
            data = data.Replace("\"isBeforeMeal\":null", "\"isBeforeMeal\":false");
            data = data.Replace("\"isControlMeasurement\":null", "\"isControlMeasurement\":false");
            data = data.Replace("\"isOutOfBounds\":null", "\"isOutOfBounds\":false");
            data = data.Replace("\"otherInformation\":null", "\"otherInformation\":false");
            return data;
        }
        //
        /// <summary>
        /// This will call the OpenTeleHTTPAPI with data that needs to be persisted at the server.
        /// Typically, this is handled using HTTP POST. IT returns a JSON response (SW)
        /// Author: Stefan Wagner
        /// </summary>
        /// <param name="url">the endpoint url to call </param>
        /// <param name="url">is this a rest or non-rest http call</param>
        /// <param name="username">the username of the user wishing to login (e.g. nancyann)</param>
        /// <param name="password">the password of the user (e.g. abcd1234)</param>        
        /// <param name="postdata">the json data to call</param>
        /// <returns>a JSON string with the result</returns>
        private string SendData(string url, bool rest, string username, string password, string postdata)
        {
            var request = (HttpWebRequest)WebRequest.Create(Url(url));
            request.ContentType = "text/json";
            request.Method="POST";
            request.Credentials = GetCredential(rest, url, username, password);
            request.PreAuthenticate = true;

            using (var streamWriter = new StreamWriter(request.GetRequestStream()))
            {

                streamWriter.Write(postdata);
                streamWriter.Flush();
                streamWriter.Close();
            }

            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            Stream receiveStream = response.GetResponseStream();
            // Pipes the stream to a higher level stream reader with the required encoding format. 
            StreamReader readStream = new StreamReader(receiveStream, Encoding.Default);
            String data = readStream.ReadToEnd();

            return data;

        }

        /// <summary>
        /// Helper method for combining the set server adress with the specific ressource
        /// Author: Stefan Wagner
        /// </summary>
        /// <param name="url">the specific resource to combine with server endpoint</param>
        /// <returns>the specific ressource url</returns>
        private string Url(string url)
        {
            return Path.Combine(Server, url);
        }

        /// <summary>
        /// Get all blood pressure mesaurements in JSON format on the specifc patient
        /// Author: Stefan Wagner
        /// </summary>
        /// <param name="username">the username of the user wishing to login (e.g. nancyann)</param>
        /// <param name="password">the password of the user (e.g. abcd1234)</param>        
        /// <returns>A raw JSON string of blood pressure data </returns>
        public string GetBloodPressureMeasuremsentsRaw(string username, string password) 
        {
            return CallOpenTeleHttpAPI(BloodPressureMeasurementListURL, false, username, password);
        }

        /// <summary>
        /// Get all blood pressure mesaurements as an object structure on the specifc patient
        /// Author: Stefan Wagner
        /// </summary>
        /// <param name="username">the username of the user wishing to login (e.g. nancyann)</param>
        /// <param name="password">the password of the user (e.g. abcd1234)</param>        
        /// <returns>A serialized object structure containing the blood pressure measurements</returns>
        public BloodPressureRootobject GetBloodPressureMeasuremsents(string username, string password)
        {

            return (Newtonsoft.Json.JsonConvert.DeserializeObject<BloodPressureRootobject>          
                (GetBloodPressureMeasuremsentsRaw(username, password)));

        }

        /// <summary>
        /// Get all blood sugar mesaurements in JSON format on the specifc patient
        /// Author: Stefan Wagner
        /// </summary>
        /// <param name="username">the username of the user wishing to login (e.g. nancyann)</param>
        /// <param name="password">the password of the user (e.g. abcd1234)</param>        
        /// <returns>A raw JSON string of blood pressure data </returns>
        public string GetBloodSugarMeasurementsRaw(string username, string password)
        {
            return CallOpenTeleHttpAPI(BloodSugarURL, false, username, password);
        }


        /// <summary>
        /// Get all blood sugar mesaurements as an object structure on the specifc patient
        /// Author: Stefan Wagner
        /// </summary>
        /// <param name="username">the username of the user wishing to login (e.g. nancyann)</param>
        /// <param name="password">the password of the user (e.g. abcd1234)</param>        
        /// <returns>A object structure containing blood sugar measurement data </returns>
        public ComplexMeasurementsRootObject GetBloodSugarMeasurements(string username, string password)
        {

            return (Newtonsoft.Json.JsonConvert.DeserializeObject<ComplexMeasurementsRootObject>
                (GetBloodSugarMeasurementsRaw(username, password)));

        }

        /// <summary>
        /// Get all FEV1 measurments in JSON format on the specifc patient
        /// Author: Stefan Wagner
        /// </summary>
        /// <param name="username">the username of the user wishing to login (e.g. nancyann)</param>
        /// <param name="password">the password of the user (e.g. abcd1234)</param>        
        /// <returns>A raw JSON string of FEV1 measurments</returns>
        public string GetFEV1MeasurementsRaw(string username, string password)
        {

            return CallOpenTeleHttpAPI(LungfunctionURL, false, username, password);
        
        }

        /// <summary>
        /// Get all FEV1 measurments as an object structure on the specifc patient
        /// Author: Stefan Wagner
        /// </summary>
        /// <param name="username">the username of the user wishing to login (e.g. nancyann)</param>
        /// <param name="password">the password of the user (e.g. abcd1234)</param>        
        /// <returns>An object structure containing FEV1 measurments</returns>
        public BasicMeasurementRootobject GetFEV1Measurements(string username, string password)
        {

            return (Newtonsoft.Json.JsonConvert.DeserializeObject<BasicMeasurementRootobject>
                (GetFEV1MeasurementsRaw(username, password)));

        }

        /// <summary>
        /// Get all weight measurments in JSON format on the specifc patient
        /// Author: Stefan Wagner
        /// </summary>
        /// <param name="username">the username of the user wishing to login (e.g. nancyann)</param>
        /// <param name="password">the password of the user (e.g. abcd1234)</param>        
        /// <returns>A raw JSON string of weight measurments</returns>
        public string GetWeightMeasurementsRaw(string username, string password)
        {
        
            return CallOpenTeleHttpAPI(WeightURL, false, username, password);
        
        }

        /// <summary>
        /// Get all weight measurments as an object structure on the specifc patient
        /// Author: Stefan Wagner
        /// </summary>
        /// <param name="username">the username of the user wishing to login (e.g. nancyann)</param>
        /// <param name="password">the password of the user (e.g. abcd1234)</param>        
        /// <returns>An object structure containing weight measurments</returns>
        public BasicMeasurementRootobject GetWeightMeasurements(string username, string password)
        {

            return (Newtonsoft.Json.JsonConvert.DeserializeObject<BasicMeasurementRootobject>
                (GetWeightMeasurementsRaw(username, password)));

        }

        /// <summary>
        /// Get all saturation measurments in JSON format on the specifc patient
        /// Author: Stefan Wagner
        /// </summary>
        /// <param name="username">the username of the user wishing to login (e.g. nancyann)</param>
        /// <param name="password">the password of the user (e.g. abcd1234)</param>        
        /// <returns>A raw JSON string of saturation measurments</returns>
        public string GetSaturationMeasurementRaw(string username, string password)
        {
        
            return CallOpenTeleHttpAPI(SaturationURL, false, username, password);

        }

        /// <summary>
        /// Get all saturation measurments as an object structure on the specifc patient
        /// Author: Stefan Wagner
        /// </summary>
        /// <param name="username">the username of the user wishing to login (e.g. nancyann)</param>
        /// <param name="password">the password of the user (e.g. abcd1234)</param>        
        /// <returns>An object structure containing saturation measurments</returns>
        public BasicMeasurementRootobject GetSaturationMeasurement(string username, string password)
        {

            return (Newtonsoft.Json.JsonConvert.DeserializeObject<BasicMeasurementRootobject>
                (GetSaturationMeasurementRaw(username, password)));

        }

        /// <summary>
        /// Get all pulse  measurments in JSON format on the specifc patient
        /// Author: Stefan Wagner
        /// </summary>
        /// <param name="username">the username of the user wishing to login (e.g. nancyann)</param>
        /// <param name="password">the password of the user (e.g. abcd1234)</param>        
        /// <returns>A raw JSON string of pulse measurments</returns>
        public string GetPulseMeasurementRaw(string username, string password)
        {
            return CallOpenTeleHttpAPI(PulseURL, false, username, password);
        }


        /// <summary>
        /// Get all pulse measurments as an object structure on the specifc patient
        /// Author: Stefan Wagner
        /// </summary>
        /// <param name="username">the username of the user wishing to login (e.g. nancyann)</param>
        /// <param name="password">the password of the user (e.g. abcd1234)</param>        
        /// <returns>An object structure containing pulse measurments</returns>
        public BasicMeasurementRootobject GetPulseMeasurement(string username, string password)
        {

            return (Newtonsoft.Json.JsonConvert.DeserializeObject<BasicMeasurementRootobject>
                (GetPulseMeasurementRaw(username, password)));

        }

       /*"name":"Lungefunktion","QuestionnaireId":43,"version":"0.1","output":[{"name":"302.LF#FEV1","type":"Float","value":4},{"name":"302.LF##SEVERITY","value":"GREEN","type":"String"}],"date":"2015-12-01T19:55:03.906Z"}*/

        /// <summary>
        /// "The FVC (Forced Vital Capacity) method is the single most important test in spirometry.
        /// The volume expired in the first second of the FVC test is called FEV1 
        /// (Forced Expiratory Volume in the first second) and is a very important parameter in spirometry.
        /// The FEV1% is the FEV1 divided by the VC (Vital Capacity: see next spirometry test on this page) times
        /// 100: FEV1%=FEV1/VC X100. This parameter is also known as the Tiffeneau index, named after the french
        /// physician that discovered the FEV1/VC ratio. Nowadays FEV1/FVC X100 is also accepted as FEV1% (FEV1 
        /// FVC ratio).
        /// A healthy patients expires approximately 80% of all the air out of his lungs in the first second
        /// during the FVC maneuver. 
        /// A patient with an obstruction of the upper airways has a decreased FEV1/FVC ratio." 
        /// Source http://www.spirometry.guru/spirometry.html    
        /// 
        /// In OpenTele2 - most data are sent as part of a questionnaire. From here, measurement data 
        /// are stored as measurements and other types of data. This is not well desribed in the OpenTele2
        /// documentation. Thus, when creating a new questionnaire, one must understand the relevant format to use
        /// More work is needed here in generalizing and simplifying the sending new data.
        /// Author: Stefan Wagner
        /// </summary>
        /// <param name="value">The FEV1 value measured</param>
        /// <param name="username">the username of the user wishing to login (e.g. nancyann)</param>
        /// <param name="password">the password of the user (e.g. abcd1234)</param>    
        /// <returns>Server returns confirmation of what has been recieved and/or saved as a string (SW)</returns>
       public String postQuestionnaireFEV1Measurement(string value, string username, string password)
        {
           // This list of hardcoded strings should either be moved to a define file, or a property file.
           // Please note, that the id and numbering (e.g. 43, and 302), might change when moving servers, due to 
           // the dynamic nature of OpenTele2. Work is underway to solve this automatically in the API (SW).
           string id = "43";
           string name = "302.LF##SEVERITY)";
           string qno = "302.LF#FEV1";
           string version = "0.1";
           string type = "Float";
          
           // Date handling is Linux/Java style - and thus we need specialized string handling (SW)
           // Better handling is desirable (SW)
           var date = DateTime.Now;
           string stringdate = string.Format("{0}-{1}-{2}T{3}.000Z", date.Year, date.Month, date.Day,
                date.ToUniversalTime().ToLongTimeString());

           // Postdata has been reverse engineered from OpenTele2 calls. (SW) 
           // Documentation in OpenTele2 on this is poor. (SW)
           var postData = 
               string.Format("{{\"name\":\"{0}\",\"QuestionnaireId\":{1},\"version\":\"{2}\",\"output\":[{{\"name\":\"{3}\",\"type\":\"{4}\",\"value\":{5}}}],\"date\":\"{6}\"}}", 
                name, id, version, qno, type, value, stringdate);

            // Server returns confirmation of what has been recieved and/or saved (SW)
           var result = SendData("rest/questionnaire/listing", true, username, password, postData);
            Console.WriteLine("Send data: result:" + postData);
            return result;
        }

        /*
         * {"name":"Saturation (manuel)","QuestionnaireId":47,"version":"0.1","output":[{"name":"316.SAT#SATURATION","value":91,"type":"Integer"},
         * {"name":"316.SAT#PULSE","value":67,"type":"Integer"},
         * {"name":"316.SAT##SEVERITY","value":"GREEN","type":"String"}],"date":"2015-12-01T21:08:47.267Z"}
         */
    /// <summary>
    /// Most oximeters measure both saturation and pulse. Thus, a typical 
    /// questionnaire for COPD and CF patients would include this measurement.
    /// Author: Stefan Wagner
    /// </summary>
    /// <param name="saturation_value">the oxygen saturation level in %</param>
    /// <param name="pulse_value">the pulse in BPM</param>
    /// <param name="username">the username of the user wishing to login (e.g. nancyann)</param>
    /// <param name="password">the password of the user (e.g. abcd1234)</param>    
    /// <returns>Server returns confirmation of what has been recieved and/or saved as a string (SW)</returns>
      public String postQuestionnaireSaturationAndPulseMeasurement(string saturation_value, 
           string pulse_value, string username, string password)
       {
           //Refactor to static strings or properties (SW)
           string id = "47"; 
           string name  = "Saturation (manuel)";
           string version = "0.1";
           string saturation = "316.SAT#SATURATION";
           string type = "Integer";
           string pulse = "316.SAT#PULSE";

           // Date handling is Linux/Java style - and thus we need specialized string handling (SW)
           // Better handling is desirable (SW)
           var date = DateTime.Now;
           string stringdate = string.Format("{0}-{1}-{2}T{3}.000Z", 
               date.Year, date.Month, date.Day, date.ToUniversalTime().ToLongTimeString());

           var postData = string.Format("{{\"name\":\"{0}\",\"QuestionnaireId\":{1},\"version\":\"{2}\",\"output\":[{{\"name\":\"{3}\",\"type\":\"{4}\",\"value\":{5}}}, {{\"name\":\"{6}\",\"type\":\"{4}\",\"value\":{7}}}],\"date\":\"{8}\"}}", name, id, version, saturation, type, saturation_value, pulse, pulse_value, stringdate);

           // Server returns confirmation of what has been recieved and/or saved
           var result = SendData("rest/questionnaire/listing", true, username, password, postData);
           Console.WriteLine("Send data: result:" + postData);
           return result;
       }

        /// <summary>
        /// This questionnaire will provide information on a blood sugar measurement session.
        /// This is typically targetting diabetes patients.
        /// It is important to understand whether the patient measured before or after a meal 
        /// 
        /// "The blood sugar concentration or blood glucose level is the amount of glucose (sugar) present in the 
        /// blood of a human or animal. The body naturally tightly regulates blood glucose levels as a part of 
        /// metabolic homeostasis. The normal blood glucose level (tested while fasting) for non-diabetics, 
        /// should be between 3.9 and 5.5 mmol/L (70 to 100 mg/dL). The mean normal blood glucose level in humans 
        /// is about 5.5 mmol/L (100 mg/dL);[6] however, this level fluctuates throughout the day. 
        /// Blood sugar levels for those without diabetes and who are not fasting 
        /// should be below 6.9 mmol/L (125 mg/dL)." Source https://en.wikipedia.org/wiki/Blood_sugar
        /// </summary>
        /// <param name="isBeforeMeal">did the patient measure before a meal</param>
        /// <param name="value">the value measured </param>
        /// <param name="username">the username of the user wishing to login (e.g. nancyann)</param>
        /// <param name="password">the password of the user (e.g. abcd1234)</param>    
        /// <returns>Server returns confirmation of what has been recieved and/or saved as a string (SW)</returns>
        public String postQuestionnaireBloodSugarMeasurement(string isBeforeMeal, 
            string value, string username, string password)
        {

            // Refactor to static strings or properties (SW)
            int id = 41;
            //288.BS#BLOODSUGARMEASUREMENTS
            string name = "Blodsukker (manuel)";
            string qno = "288.BS#BLOODSUGARMEASUREMENTS";
            string runningnumber = "288.BS##SEVERITY";
            
            string version = "0.1";
            string type = "BloodSugarMeasurements";

            // Date handling is Linux/Java style - and thus we need specialized string handling (SW)
            // Better handling is desirable (SW)
            var date = DateTime.Now;
            string stringdate = string.Format("{0}-{1}-{2}T{3}.000Z", 
                date.Year, date.Month, date.Day, date.ToUniversalTime().ToLongTimeString());

            var postData = string.Format("{{\"name\":\"{0}\",\"QuestionnaireId\":{1},\"version\":\"{2}\",\"output\":[{{\"name\":\"{3}\",\"type\":\"{4}\",\"value\":{{\"measurements\":[{{\"result\":{5},\"isBeforeMeal\":{6},\"timeOfMeasurement\":\"{7}\"}}],\"transferTime\":\"{7}\"}}}},{{\"name\":\"{8}\",\"value\":\"GREEN\",\"type\":\"String\"}}],\"date\":\"{7}\"}}", name, id, version, qno, type, value, isBeforeMeal, stringdate, runningnumber);
            /*
                {"name":"Blodsukker (manuel)","QuestionnaireId":41,"version":"0.1","output":[{"name":"288.BS#BLOODSUGARMEASUREMENTS","type":"BloodSugarMeasurements","value":{"measurements":[{"result":6,"isBeforeMeal":true,"timeOfMeasurement":"2015-11-27T21:43:19.000Z"}],
                "transferTime":"2015-11-27T21:43:19.000Z"},{"name":"288.BS##SEVERITY","value":"GREEN","type":"String"}],"date":"2015-11-27T21:43:19.000Z"}
             
             */
            // Server returns confirmation of what has been recieved and/or saved
            var result = SendData("rest/questionnaire/listing", true, username, password, postData);
            Console.WriteLine("Send data: result:" + postData);
            return result;
        }

        /// <summary>
        /// This will provide the relevant credentials to the OpenTele citizen server.
        /// The server uses Basic Authentication with a username and a password.
        /// Author: Stefan Wagner
        /// </summary>
        /// <param name="rest">is this a rest based web service?</param>
        /// <param name="url">getting the url to get credentials for</param>
        /// <param name="username">the username of the user wishing to login (e.g. nancyann)</param>
        /// <param name="password">the password of the user (e.g. abcd1234)</param>    
        /// <returns>An interface to the credentials</returns>
        private ICredentials GetCredential(bool rest, string url, string username, string password)
        {
            url = Url(url);
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3;
            CredentialCache credentialCache = new CredentialCache();
            if (rest)
            {
                credentialCache.Add(new System.Uri(url), "Basic", new NetworkCredential(username, password, ""));
            }
            else
            {

                credentialCache.Add(new System.Uri(url), "Basic", new NetworkCredential(username, password));
            }
            return credentialCache;
        }
    }
}
