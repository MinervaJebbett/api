# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary

This is the first version of a C# based .NET wrapper for the OpenTele2 web API. Focus is on the patient API, but work is also commencing on wrapping the clinical interface. The patient API allows for getting and sending questionnaire data belonging to a patient:  Questionnaire data, automated or manual medical device measurements, and subjective observations as well as for exchanging text messages between the clinicians.  The OpenTele project is made to promote the use of telemedicine, by illustrating how monitoring and training can happen remotely. It is part of the national Danish strategy for telemedicine, and it is being used in such diverse areas as pre-eclampsia monitoring, COPD, diabetes, and patients suffering from depression and dementia. This .NET API has been created to support moving to other platforms, including Windows .NET and Linux Mono.Net frameworks, to supplement the current Android and HTML5 based clients.  The OpenTele2 project is open source under the Apache 2.0 license - and thus this wrapper falls under the license to be fully compatible. Creating alternative user interfaces for both patients and clinicians is important, as well as for integrating with third party medical devices and systems. Thus, we are also working  on HL7 integration (FHIR and more).The provided solution include a source project for data transfer objects called "OpenTele.DTO".   This builds to an assembly (dll) called "OpenTele.DTO.dll". This dll may be shared across different layers (GUI, BLL, DAL) but may also be used for creating distributed wrappers, e.g. HL7 FHIR based wrappers. Basically, use this for domain understanding. However, OpenTeleNet.API also allows for raw JSON style communication as an alternative. Thus, most methods exists in both a raw JSON format, as well as an  object oriented (data transfer object) approach. The core of the API is the OpenTeleNet.API project and the resulting assembly "OpenTeleNet.API.dll".Also in the API you can find the OpenTeleConsoleDemo project - which will provide an executable that will demo how to use the API. The OpenTeleConsoleDemo project uses the two dlls "OpenTele.DTO.dll" and "OpenTeleNet.API.dll" to work.  For more information on OpenTele2 - please visit: http://4s-online.dk/wiki/doku.php?id=opentele:overview. The authors retain all rights to use the provided code and concepts for other purposes, both academic, teaching, research, and commercial use. In case of questions, please contact Stefan Wagner at sw@eng.au.dk

Many people start using medical products and sleeping tablets( https://www.ukmeds.co.uk/treatments/sleeping-tablets/ ) for releasing stress and depression. It is important to get relaxation from anxiety and stress for a good and healthy life but not by using medicines and sleeping pills. People should use physical treatments like exercise daily and head massage for treating depression without any side effect. Sleeping pills have many side effects like dizziness, inactiveness, diarrhea, etc.


* Version

### How do I get set up? ###

* Summary of set up

You need .NET SDK to build the code. Optimally, a version of Visual Studio 2012 or similar. If you do not have NuGet support, you will need to download NewtonSoft JSON assembly dll.
Start with downloading the source code from here: https://bitbucket.org/opentelenet/api/downloads - or cloning it using your GIT client.
Open the solution is Visual Studio or other editor - select: OpenTeleIntegration.sln
Build the project and run it F5 or run. The demo project should start up automatically.

* Configuration

No further configuration is needed.

* Dependencies

Newtonsoft JSON assembly is needed.
OpenTele2 demo server must be up and running. By default, the API uses the endpoint http://opentele-citizen.4s-online.dk/ from the 4S organization

* How to run tests

The OpenTeleConsoleDemo project (listed as default project) will create an executable (OpenTeleConsoleDemo.exe) - that will illustrate how the API is used.

* Deployment instructions

Remember to have access to the internet, and verify that either the 4S OpenTele server is running, or point to another server location.
http://opentele-citizen.4s-online.dk/

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin

Stefan Wagner, Aarhus University, Denmark
Email: sw@eng.au.dk

* Other community or team contact
Frederik Vincent Enghave Larsen
Email: TBD